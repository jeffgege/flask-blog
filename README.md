# Python Blog

This is a project for my blog website. In the past I used a static site generator Hugo. It was a temporary solution until I built my own. You can see the website [here](http://www.jeffgege.com).