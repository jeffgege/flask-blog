#!/bin/bash
# clear screen
clear
# Check for files
if [ ! -f /app/config.py ]
then
  echo "You must create a config file in this directory. Please copy the config.py.sample and fill in the information"
  exit 1
fi
ln -fs /usr/share/zoneinfo/America/Phoenix /etc/localtime
pip install -r requirements.txt
if [ "$DB_UPGRADE" = "true" ]
then
  echo "Upgrading database"
  # Init is only needed on new databases
  python /app/run.py db init
  python /app/run.py db migrate -m "$DB_UPGRADE_MSG"
  python /app/run.py db upgrade
  echo "Database upgrade completed"
fi
if [ "$ENV_TYPE" = "prod" ]
then
  gunicorn --config /app/gunicorn_config.py run:app --log-file /app/logs/gunicorn.log
else
  python /app/run.py run
fi