from flask import render_template, redirect, url_for, flash, session, current_app, request
from flask_login import login_user, logout_user, current_user, login_required
from app.admin import bp
from datetime import datetime
from app import db
from app.models import User, BlogPost, AboutMe
from app.admin.forms import CreatePost, EditAboutMe
from sqlalchemy import exc


@bp.route('/', methods=['GET'])
@login_required
def index():
    return "None"


@bp.route('/add_post', methods=['GET', 'POST'])
@login_required
def add_post():
    form = CreatePost()
    if form.validate_on_submit():
        if request.method == 'POST':
            post_body = request.form.get('ckeditor')
        else:
            post_body = None
        if form.draft.data is True:
            hidden = True
        else:
            hidden = False
        try:
            new_post = BlogPost(title=form.title.data, body=post_body, draft=form.draft.data,
                                date_created=datetime.now().strftime('%Y-%m-%d %H:%M:%S'), author=session['user_id'],
                                hidden=hidden, times_loaded=0)
            db.session.add(new_post)
            db.session.commit()
        except exc.SQLAlchemyError:
            current_app.logger.exception("Unable to add new blog post")
            flash("Unable to add new blog post")
        return redirect(url_for('main.index'))
    return render_template('admin/new_post.html', title='Create Blog Post', form=form)


@bp.route('/edit_about_me', methods=['GET', 'POST'])
@login_required
def edit_about_me():
    form = EditAboutMe()
    about_me = None
    about_me_body = None
    try:
        about_me = AboutMe.query.order_by(AboutMe.id.desc()).limit(1).first()
    except exc.SQLAlchemyError:
        current_app.logger.exception("Unable to get latest about me")
    except exc.NoResultFound:
        about_me = None
    if form.validate_on_submit():
        post_body = None
        if request.method == 'POST':
            post_body = request.form.get('ckeditor')
        try:
            new_about_me = AboutMe(title=form.title.data, body=post_body, git_url=form.git_url.data,
                                   linkedin_url=form.linkedin_url.data, twitter_url=form.twitter_url.data,
                                   email_addr=form.email_address.data, date_updated=datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
            db.session.add(new_about_me)
            db.session.commit()
        except exc.SQLAlchemyError:
            current_app.logger.exception("Unable to add new About Me")
            flash("An error occured updating the about me page. Please try again, or contact the site admin.")
        return redirect(url_for('admin.edit_about_me'))
    if about_me is not None:
        form.title.data = about_me.title
        about_me_body = about_me.body
        form.git_url.data = about_me.git_url
        form.linkedin_url.data = about_me.linkedin_url
        form.twitter_url.data = about_me.twitter_url
        form.email_address.data = about_me.email_addr
    return render_template("admin/edit_about_me.html", title="Edit About Me", form=form, about_me_body=about_me_body)
