from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField, IntegerField, SelectField, BooleanField
from wtforms.validators import ValidationError, DataRequired, Email, EqualTo, URL


class CreatePost(FlaskForm):
    title = StringField("Post Title", validators=[DataRequired()])
    draft = BooleanField("Is this a draft?")
    submit = SubmitField("Save")


class EditAboutMe(FlaskForm):
    title = StringField("About Me Title")
    git_url = StringField("GIT URL", validators=[URL()])
    linkedin_url = StringField("LinkedIn URL", validators=[URL()])
    twitter_url = StringField("Twitter URL", validators=[URL()])
    email_address = StringField("Email Address", validators=[Email()])
    submit = SubmitField("Save")