import os
from time import time
from flask import current_app, url_for
from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash
import jwt
from app import db, login
from datetime import datetime, timedelta
from sqlalchemy.orm import relationship
import base64


class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    first_name = db.Column(db.String(64))
    last_name = db.Column(db.String(64))
    password_hash = db.Column(db.String(120), index=True)
    email = db.Column(db.String(120), index=True, unique=True)
    date_created = db.Column(db.DateTime)
    last_seen = db.Column(db.DateTime)
    token = db.Column(db.String(32), index=True, unique=True)
    token_expiration = db.Column(db.DateTime)
    permission = db.Column(db.Integer)
    enabled = db.Column(db.Boolean)

    def __repr__(self):
        return '<User {}>'.format(self.username)

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    def get_reset_token(self, expires_in=600):
        return jwt.encode(
            {'reset_password': self.id, 'exp': time() + expires_in},
            current_app.config['SECRET_KEY'],
            algorithm='HS256').decode('UTF-8')

    def is_enabled(self):
        return self.enabled

    def get_permission(self):
        return self.permission

    @staticmethod
    def verify_password_token(token):
        try:
            id = jwt.decode(token, current_app.config['SECRET_KEY'], algorithm=['HS256'])['reset_password']
        except:
            return
        return User.query.get(id)

    def get_token(self, expires_in=3600):
        now = datetime.utcnow()
        if self.token and self.token_expiration > now + timedelta(seconds=60):
            return self.token
        self.token = base64.b64encode(os.urandom(24)).decode('utf-8')
        self.token_expiration = now + timedelta(seconds=expires_in)
        db.session.add(self)
        return self.token

    def revoke_token(self):
        self.token_expiration = datetime.utcnow() - timedelta(seconds=1)

    def is_admin(self):
        if self.permission == 1:
            return True
        else:
            return False

    def get_users_name(self):
        return self.first_name+" "+self.last_name

    @staticmethod
    def check_token(token):
        user = User.query.filter_by(token=token).first()
        if user is None or user.token_expiration < datetime.utcnow():
            return None
        return user


@login.user_loader
def load_user(id):
    return User.query.get(int(id))


class BlogPost(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(120), index=True)
    body = db.Column(db.Text)
    draft = db.Column(db.Integer)
    date_created = db.Column(db.DateTime)
    date_updated = db.Column(db.DateTime)
    author = db.Column(db.Integer, db.ForeignKey('user.id'), index=True)
    tags = db.Column(db.String(120))
    times_loaded = db.Column(db.Integer, default=0)
    # Set blog post to hidden if value is 1
    hidden = db.Column(db.Integer, default=0)


class BlogComments(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    post_id = db.Column(db.Integer, db.ForeignKey('blog_post.id'), index=True)
    written_by = db.Column(db.Integer, db.ForeignKey('user.id'), index=True)
    comment_text = db.Column(db.Text)
    timestamp = db.Column(db.DateTime)
    # Set comment to hidden if value is 1
    hidden = db.Column(db.Integer, default=0)


class AboutMe(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(64))
    body = db.Column(db.Text)
    git_url = db.Column(db.String(120))
    linkedin_url = db.Column(db.String(120))
    twitter_url = db.Column(db.String(120))
    email_addr = db.Column(db.String(120))
    date_updated = db.Column(db.String(120))
