from flask import render_template, redirect, url_for, flash, session, current_app
from flask_login import login_user, logout_user, current_user, login_required
from app.main import bp
from datetime import datetime
from app import db
from app.models import User, BlogPost, AboutMe
from sqlalchemy import exc
from bs4 import BeautifulSoup


@bp.route('/', methods=['GET'])
def index():
    no_posts = False
    blog_posts = None
    preview_list = {}
    try:
        blog_posts = BlogPost.query.filter(BlogPost.hidden!=1).order_by(BlogPost.id.desc()).limit(5).all()
    except exc.SQLAlchemyError:
        current_app.logger.exception("Unable to get blog posts")
    except exc.NoResultFound:
        no_posts = True
        blog_posts = None
    for post in blog_posts:
        soup = BeautifulSoup(post.body, features="html.parser")
        preview_list[post.id] = soup.get_text()[:400]
    return render_template("main/index.html", blog_posts=blog_posts, no_posts=no_posts, preview_list=preview_list)


@bp.route('/about_me', methods=['GET'])
def about_me():
    about_me = None
    try:
        about_me = AboutMe.query.order_by(AboutMe.id.desc()).limit(1).first()
    except exc.SQLAlchemyError:
        current_app.logger.exception("Unable to get latest about me")
    except exc.NoResultFound:
        about_me = None
    return render_template("main/about_me.html", about_me=about_me, title='About Me')


@bp.route('/post/<post_id>', methods=['GET'])
def post(post_id):
    error = None
    blog_post = BlogPost.query.filter(BlogPost.id==post_id, BlogPost.hidden!=1).first()
    try:
        if blog_post is None:
            error = "Unable to find blog post. Please try again or contact the site admin."
        else:
            blog_post.times_loaded += 1
            db.session.add(blog_post)
            db.session.commit()
    except exc.SQLAlchemyError:
        current_app.logger.exception("Unable to update blog post read count")
    except exc.NoResultFound:
        error = "Unable to find blog post. Please try again or contact the site admin."
    return render_template("main/post.html", blog_post=blog_post, error=error)


@bp.route('/all_posts', methods=['GET'])
def all_posts():
    no_posts = False
    blog_posts = None
    preview_list = {}
    try:
        blog_posts = BlogPost.query.filter(BlogPost.hidden!=1).order_by(BlogPost.id.desc()).all()
    except exc.SQLAlchemyError:
        current_app.logger.exception("Unable to get blog posts")
    except exc.NoResultFound:
        no_posts = True
        blog_posts = None
    for post in blog_posts:
        soup = BeautifulSoup(post.body, features="html.parser")
        preview_list[post.id] = soup.get_text()[:400]
    return render_template("main/all_posts.html", blog_posts=blog_posts, no_posts=no_posts, preview_list=preview_list)
