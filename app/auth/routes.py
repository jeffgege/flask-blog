from flask import render_template, redirect, url_for, flash, session, current_app
from flask_login import login_user, logout_user, current_user, login_required
from app.auth import bp
from app.auth.forms import LoginForm, ResetPasswordForm, ResetPasswordRequestForm, RegistrationForm
from datetime import datetime
from app import db
from app.models import User
from sqlalchemy import exc


@bp.route('/login', methods=['GET', 'POST'])
def login():
    try:
        if User.query.first() is None:
            current_app.logger.info(str(User.query.first()))
            current_app.logger.info("Running installer")
            return redirect(url_for("install.index"))
    except exc.SQLAlchemyError:
        # Should have redirected but if not redirect to installer anyways
        current_app.logger.exception("Running installer")
        return redirect(url_for("install.index"))
    if current_user.is_authenticated:
        return redirect(url_for('main.index'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user is None or not user.check_password(form.password.data) or user.is_enabled() is not True:
            flash('Invalid username or password')
            return redirect(url_for('auth.login'))
        login_user(user)
        session['user_id'] = current_user.get_id()
        session['username'] = form.username.data
        session['first_name'] = user.first_name
        session['last_name'] = user.last_name
        session['permission'] = user.permission
        try:
            user.last_seen = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            db.session.add(user)
            db.session.commit()
        except exc.SQLAlchemyError:
            current_app.logger.exception("Unable to update last seen")
            flash("An unexpected error has occurred. Please try again later")
            return redirect(url_for('auth.login'))
        return redirect(url_for('main.index'))
    return render_template('auth/login.html', title='Login', form=form)


@login_required
@bp.route('/register', methods=['GET', 'POST'])
def register():
    form = RegistrationForm()
    if form.validate_on_submit():
        user = User(username=form.username.data, email=form.email.data, first_name=form.first_name.data,
                    last_name=form.last_name.data, date_created=datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
                    permission=2, enabled=1)
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        flash('User has been created')
        return redirect(url_for('auth.login'))
    return render_template('auth/register.html', title='Register', form=form)


@login_required
@bp.route('/logout', methods=['GET', 'POST'])
def logout():
    logout_user()
    return redirect(url_for('auth.login'))


# @bp.route('/reset_password_request', methods=['GET', 'POST'])
# def reset_password_request():
#     if current_user.is_authenticated:
#         return redirect(url_for('main.index'))
#     form = ResetPasswordForm()
#     if form.validate_on_submit():
#         user = User.query.filter_by(email=form.email.data).first()
#         # todo build reset email functionality
#         flash('This module is not built yet. Please contact support.')
#         return redirect(url_for('auth.login'))
#     return render_template('auth/reset_password_request.html', title='Reset Password', form=form)
#
#
# @bp.route('/reset_password/<token>', methods=['GET', 'POST'])
# def reset_password(token):
#     if current_user.is_authenticated:
#         return redirect(url_for('main.index'))
#     user = User.verify_password_token(token)
#     if not user:
#         return redirect(url_for('main.index'))
#     form = ResetPasswordForm()
#     if form.validate_on_submit():
#         user.set_password(form.password.data)
#         db.session.commit()
#         flash('Password has been reset')
#         return redirect(url_for('auth.login'))
#     return render_template(url_for('auth/reset_password.html', form=form))
